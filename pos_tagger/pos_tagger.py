from .config import Config
from .pos_model import PosModel


def pos_tagger():
    """
    Initialize POS tagger model.
    POS tagger model created by Guillaume Genthial.
    https://github.com/guillaumegenthial/sequence_tagging
    """
    config = Config()
    model = PosModel(config)
    model.build()
    model.restore_session(config.dir_model)

    return model
