import os

from .data_utils import get_trimmed_glove_vectors, load_vocab, \
        get_processing_word


class Config:
    def __init__(self, load=True):
        """Initialize hyperparameters and load vocabs

        Args:
            load_embeddings: (bool) if True, load embeddings into
                np array, else None

        """
        # directory for training outputs
        if not os.path.exists(self.dir_output):
            os.makedirs(self.dir_output)

        # load if requested (default)
        if load:
            self.load()

    def load(self):
        """Loads vocabulary, processing functions and embeddings

        Supposes that build_data.py has been run successfully and that
        the corresponding files have been created (vocab and trimmed GloVe
        vectors)

        """
        # 1. vocabulary
        self.vocab_words = load_vocab(self.filename_words)
        self.vocab_tags = load_vocab(self.filename_tags)
        self.vocab_chars = load_vocab(self.filename_chars)

        self.nwords = len(self.vocab_words)
        self.nchars = len(self.vocab_chars)
        self.ntags = len(self.vocab_tags)

        # 2. get processing functions that map str -> id
        self.processing_word = get_processing_word(self.vocab_words, self.vocab_chars,
                                                   lowercase=True, chars=self.use_chars)
        self.processing_tag = get_processing_word(self.vocab_tags, lowercase=False, allow_unk=False)

        # 3. get pre-trained embeddings
        self.embeddings = (get_trimmed_glove_vectors(self.filename_trimmed) if self.use_pretrained else None)

    # general config
    dir_output = "pos_weights/train/"
    dir_model = dir_output + "model.weights/"
    path_log = dir_output + "log.txt"

    # embeddings
    dim_word = 200
    dim_char = 100

    # glove files
    filename_glove = "data/glove5.100.{}_lower.txt".format(dim_word)
    filename_trimmed = "data/glove5.100.{}_lower.trimmed.npz".format(dim_word)
    use_pretrained = True

    max_iter = None  # if not None, max number of examples in Dataset

    filename_tags = "data_pos_tagging/tags.txt"
    filename_words = "data_pos_tagging/words.txt"
    filename_chars = "data_pos_tagging/chars.txt"

    # training
    train_embeddings = False
    nepochs = 50
    dropout = 0.5
    batch_size = 20
    lr_method = "adam"
    lr = 0.001
    lr_decay = 0.9
    clip = 5
    nepoch_no_imprv = 3

    # model hyperparameters
    hidden_size_char = 100  # lstm on chars
    hidden_size_lstm = 200  # lstm on word embeddings

    # NOTE: if both chars and crf, only 1.6x slower on GPU
    use_crf = True  # if crf, training is 1.7x slower on CPU
    use_chars = True  # if char embedding, training is 3.5x slower on CPU
