from flask import Flask, request, render_template, Response, jsonify
from werkzeug.utils import secure_filename
from sklearn.externals import joblib
from keras.models import load_model
import numpy as np
import tensorflow as tf

from pos_tagger.pos_tagger import pos_tagger
from prepare_data.preprocess_data import get_tokens, sent2crf_features, prepare_data_for_rnn

ALLOWED_EXTENSIONS = ['txt']

app = Flask(__name__)

pos_model = pos_tagger()
crf_model = joblib.load('trained_models/trained_crf.pkl')
rnn_model = load_model('trained_models/sgd_crossentropy.h5', custom_objects={'f1_score': f1_score})

with open('data/words_list.txt', 'r', encoding='utf-8') as f:
    words_list = [word.strip() for word in f.read().split('\n')]
word2idx = {w: i for i, w in enumerate(words_list)}

with open('data/visi_suanotuoti_mwe.txt', 'r', encoding='utf-8') as f:
    known_mwe = [word.strip() for word in f.read().split('\n')]


def f1_score(y_true, y_pred):
    """
    F1-Score measure used for Keras RNN model.
    :param y_true: True values
    :param y_pred: Predicted values
    :return: F1-Score
    """
    y_true = tf.cast(y_true, "int32")
    y_pred = tf.cast(tf.round(y_pred), "int32")
    y_correct = y_true * y_pred
    sum_true = tf.reduce_sum(y_true, axis=1)
    sum_pred = tf.reduce_sum(y_pred, axis=1)
    sum_correct = tf.reduce_sum(y_correct, axis=1)
    precision = sum_correct / sum_pred
    recall = sum_correct / sum_true
    f_score = 2 * precision * recall / (precision + recall)
    f_score = tf.where(tf.is_nan(f_score), tf.zeros_like(f_score), f_score)

    return tf.reduce_mean(f_score)


def allowed_file(filename):
    """
    Validates file extension
    :param filename: String filename
    :return: boolean
    """
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/upload', methods=['POST'])
def submit_text_file():
    """
    Takes user input file and returns appropriate response: errors or result object.
    """
    if 'file' not in request.files:
        return Response('No file uploaded', status=500)
    file = request.files['file']
    if file.filename == '':
        return Response('No filename uploaded', status=500)
    filename = secure_filename(file.filename)
    if file and allowed_file(filename):
        file_text_string = file.read()
        result = get_results_file(file_text_string.decode('utf-8'))
        return jsonify({"result": result})
    else:
        return Response('Only text files are allowed', status=500)


def get_results_file(string_text):
    """
    Calls preprocessing functions, classification models and results merging function.
    :param string_text: Decoded user input
    :return: Results string
    """
    global word2idx

    tokenized_input = get_tokens(string_text)
    input_plus_pos = tag_pos(tokenized_input)

    input2crf = [sent2crf_features(s) for s in input_plus_pos]
    crf_predictions = crf_model.predict(input2crf)  # [['O', 'O', 'O'], ['O', 'O']]

    rnn_input = prepare_data_for_rnn(word2idx, input_plus_pos)
    rnn_predictions = rnn_model.predict(rnn_input)
    rnn_predictions = np.argmax(rnn_predictions, axis=-1)

    tagged_known = tag_from_list(tokenized_input, known_mwe)

    return merge(tokenized_input, crf_predictions, rnn_predictions, tagged_known)


def merge(tokenized_input, crf_predictions, rnn_predictions, tagged_known):
    """
    Merge results from all taggers to one result.
    :param tokenized_input: user file input in format: list of sentences, sentence is list of words
    :param crf_predictions: CRF model predictions in list of lists format
    :param rnn_predictions: RNN model predictions in list of lists format
    :param tagged_known:    Tagged known MWE in list of lists format
    :return: merged all predictions, formatted string
    """
    tags_list = ['B-MWE', 'I-MWE', 'O']
    result = []
    for words, tags, tags2, tags3 in zip(tokenized_input, crf_predictions, rnn_predictions, tagged_known):
        for word, tag, tag2, tag3 in zip(words, tags, tags2, tags3):
            if ('MWE' in tag) or ('MWE' in tags_list[tag2]) or (tag3 == 'MWE'):
                result.append('{}\tMWE\n'.format(word))
            else:
                result.append('{}\tO\n'.format(word))
        result.append('\n')

    return ''.join(result)


def tag_from_list(tokenized_input, known_mwe):
    """
    Tags multiword expressions from known source.
    :param tokenized_input: List of lists of words
    :param known_mwe:       List of known multiwords
    :return: words tagged as MWE or O (list of lists)
    """
    tagged = []
    for sent in tokenized_input:
        result = ['O' for i in sent]
        for pattern in known_mwe:
            indexes = find_sub_list_in_list(pattern, sent)
            if indexes:

                for i_list in indexes:
                    for i in range(i_list[0], i_list[1] + 1):
                        result[i] = 'MWE'
        tagged.append(result)

    return tagged


def tag_pos(input_data):
    """
    Tag part of speech for input data.
    :param input_data: '[[word1, word2, ...], [...], ...]'
    :return: [[(word, pos), (word2, pos2), ...], [...], ...]
    """
    result = []
    for sent in input_data:
        pos_tags = pos_model.predict(sent)
        result.append([(word, pos) for word, pos in zip(sent, pos_tags)])

    return result


def find_sub_list_in_list(sublist, full_list):
    """
    Find sublists in a list.
    :param sublist:   sublist to find in full list
    :param full_list: list
    :return: indexes of elements found as sublist
    """
    results = []
    sll = len(sublist)
    for ind in (i for i, e in enumerate(full_list) if e == sublist[0]):
        if full_list[ind:ind + sll] == sublist:
            results.append((ind, ind + sll - 1))

    return results


if __name__ == '__main__':
    app.run()
