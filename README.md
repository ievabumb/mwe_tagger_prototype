## Lithuanian multi-word expressions tagger prototype

### Requirements
* flask
* nltk
* numpy
* sklearn
* h5py
* gensim
* sklearn_crfsuite
* tensorflow
* keras

Also you need to download punkt data for NLTK using `nltk.download('punkt')`

### Launching the project

Part of the data because of the large size is not added to the repository. 
You can find it [here](https://drive.google.com/open?id=1RmjDqQUypocb0WlpBfZ80Lz_3T7eZNos)
Place archive content to appropriate directories according to their names.

To run a project you need to install all the required libraries and run command `python3 app.py` in a terminal.
It is recommended to run a project in Python virtual environment.
