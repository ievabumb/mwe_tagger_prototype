import numpy as np
from nltk import sent_tokenize
from nltk.tokenize import wordpunct_tokenize
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical


def get_tokens(plain_text):
    """
    Split text to sentences, sentences to words.
    :param plain_text: Plain text input
    :return: List of lists of words
    """
    sentence_string = plain_text.lower()
    sentences = sent_tokenize(sentence_string)
    sent_words = [wordpunct_tokenize(sent) for sent in sentences]

    return sent_words


def word2crf_features(sent, i):
    """
    Convert word to features for CRF method.
    :param sent: Sentence (list of words)
    :param i:    Index of word to preprocess
    :return: i-th word features
    """
    word = sent[i][0]
    sent_len = len(sent)

    features = {
        'bias': 1.0,
        'word.lower()': word,
        'word[:3]': word[:3],
        'word[-3:]': word[-3:],
        'postag': sent[i][1],
    }

    if i > 0:
        word1 = sent[i - 1][0]
        features.update({
            '-1:word.lower()': word1,
            '-1:word[:3]': word1[:3],
            '-1:word[-3:]': word1[-3:],
            '-1:postag': sent[i - 1][1],
        })
    else:
        features['BOS'] = True

    if i > 1:
        word1 = sent[i - 2][0]
        features.update({
            '-2:word.lower()': word1,
            '-2:word[:3]': word1[:3],
            '-2:word[-3:]': word1[-3:],
            '-2:postag': sent[i - 2][1],
        })
    else:
        features['BOS'] = True

    if i < sent_len - 1:
        word1 = sent[i + 1][0]
        features.update({
            '+1:word.lower()': word1,
            '+1:word[-3:]': word1[-3:],
            '+1:word[:3]': word1[:3],
            '+1:postag': sent[i + 1][1],
        })
    else:
        features['EOS'] = True

    if i < sent_len - 2:
        word1 = sent[i + 2][0]
        features.update({
            '+2:word.lower()': word1,
            '+2:word[:3]': word1[:3],
            '+2:word[-3:]': word1[-3:],
            '+2:postag': sent[i + 2][1],
        })
    else:
        features['EOS'] = True

    return features


def sent2crf_features(sent):
    """
    Loop through words in a sentence and apply feature generation function for each word.
    :param sent: Sentence (list of words)
    :return: Sentence (list of words features)
    """
    return [word2crf_features(sent, i) for i in range(len(sent))]


def prepare_data_for_rnn(word2idx, input_data):
    """
    Converts data to format suitable for RNN method: words are mapped to their indexes and POS tags are
    One Hot Encoded.
    :param word2idx:    Dictionary of words and their indexes
    :param input_data:  List of sentences. Sentence: list of tuples (word, POS)
    :return: prepared data in a list of 2 numpy arrays. First represents words as words indexes for mapping words
    embeddings weights, second represents POS tags in One Hot Encoding
    """
    max_len = 139
    n_words = len(word2idx)
    pos = ['ADP', 'ADJ', 'SCONJ', 'INTJ', 'NUM', 'VERB', 'PUNCT', 'X', 'ADV', 'PRON', 'PROPN', 'NOUN',
           'PART', 'CCONJ', 'O']
    n_pos = len(pos)
    pos2idx = {t: i for i, t in enumerate(pos)}

    to_predict_words = [[word2idx[w[0]] if w[0] in word2idx else word2idx["<unk>"] for w in s] for s in input_data]
    to_predict_words = pad_sequences(maxlen=max_len, sequences=to_predict_words, padding="post", value=n_words - 1)

    to_predict_pos = [[pos2idx[w[1]] for w in s] for s in input_data]
    to_predict_pos = pad_sequences(maxlen=max_len, sequences=to_predict_pos, padding="post", value=pos2idx["O"])
    to_predict_pos = [to_categorical(i, num_classes=n_pos) for i in to_predict_pos]

    return [to_predict_words, np.array(to_predict_pos)]
