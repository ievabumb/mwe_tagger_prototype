$('document').ready(function () {

    $("#input-id").fileinput({
            language: 'lt',
            uploadUrl: '/upload',
            theme: 'fa',
            maxFileCount: 1,
            allowedFileTypes: ['text'],
            allowedFileExtensions: ['txt'],
            uploadAsync: true,
            browseOnZoneClick: true,
            autoReplace: true,
            elErrorContainer: '#kv-error-1',
            previewFileIcon: '<i class="fa fa-file"></i>',
            maxFileSize: 200
        }
    ).on('filepreupload', function (event, data, previewId, index, jqXHR) {
            var fname = data.files[index].name;
            if (!fname.endsWith('.txt')) {
                return {
                    message: 'Galite įkelti tik tekstinius failus!'
                };
            }
        }
    ).on('filebatchpreupload', function (event, data, id, index) {
            $('#kv-success-1').html('<h4>Įkėlimo būsena</h4><ul></ul>').hide();
        }
    ).on('fileuploaded', function (event, data, id, index) {
        var fname = data.files[index].name,
            out = '<li>' + 'Failas # ' + (index + 1) + ' - ' + fname + ' įkeltas sėkmingai.' + '</li>';
        $('#kv-success-1').append(out).fadeIn('slow');
        download('MWE-' + fname, data.response.result);
    })
    ;

    function download(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
});

